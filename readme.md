# Tutorial Lab - Introducción a las Simulaciones Numéricas
## FCEN - 2021

### Descripción

Esto es un repositorio en el que queda registro del trabajo hecho y el código utilizado para resolver el primer trabajo práctico de la materia Introducción a las Simulaciones Numéricas en Ciencias Básicas, en FCEN UnCuyo. 

Dentro de los directorios hay:
 - `scripts`: Todos los scripts de las simulaciones, en su versión final.
 - `output`: Aquí hay algunos output de LAMMPS, imágenes y video. 
 - `plots`: Plots de los datos, hechos en Python.
 - `ovito`: Más análisis de datos, hecho en OVITO.
 - `latex`: Código del trabajo práctico final.
 - `logs` : Registro de algunos de los logs del trabajo.

Todo el desarrollo está contenido en el notebook, desde ahí se ejecutaron todos los comandos y en donde se realizó toda la exploración, asique recomiendo empezar por ahí. 

